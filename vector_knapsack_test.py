from gurobipy import *


def Generating_Knapsac_Example():
    import random

    # random.seed(1)
    dimension = 8
    B = [100] * dimension
    I = [[0] * dimension] * 200
    W = [0] * len(I)
    for i in range(len(I)):
        for j in range(dimension):
            I[i][j] = random.randint(5, 15)
        W[i] = random.randint(1, 10)
    return I, B, W


def Solving_Knapsack(I, B, W):
    knapsack = Model("KP")  # knapsack sub-problem
    knapsack.ModelSense = -1  # maximize
    x = {}
    for i in range(len(I)):
        x[i] = knapsack.addVar(obj=W[i], vtype="I", name="x[%d]" % i)
    knapsack.update()

    # assignment constraints
    for j in range(len(B)):
        var = [x[i] for i in range(len(I))]
        coef = [I[i][j] for i in range(len(I))]
        knapsack.addConstr(LinExpr(coef, var), "<=", B[j], name="cnstr1[%d]" % j)
    knapsack.update()
    knapsack.optimize()
    print "objective of knapsack problem:", knapsack.ObjVal

    for v in knapsack.getVars():
        print('%s %g' % (v.varName, v.x))


if __name__ == "__main__":
    I, B, W = Generating_Knapsac_Example()
    print('I: ' + str(I))
    print('B: ' + str(B))
    print('W: ' + str(W))

    Solving_Knapsack(I, B, W)
