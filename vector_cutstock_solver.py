"""
cutstock.py:  use gurobi for solving an variant of cutting stock problem, we call this problem vector cutting stock problem.

The instance of the cutting stock problem is represented by the two
lists of m items of size and quantity s=(s_i) and q=(q_i).

The roll size is B.

Given packing patterns t_1, ...,t_k,...t_K where t_k is a vector of
the numbers of items cut from a roll, the problem is reduced to the
following LP:
    
    minimize   sum_{k} x_k
    subject to sum_{k} t_k(i) x_k >= q_i    for all i
	       x_k >=0			    for all k.

We apply a column generation approch (Gilmore-Gomory approach) in
which we generate cutting patterns by solving a knapsack sub-problem.

Copyright (c) by Jiyuanshi at Southeast University, 2010
"""

import math, random, os, csv
from gurobipy import *

LOG = True
EPS = 1.e-6


def CuttingStockExample1():
    """CuttingStockExample1: create toy instance for the cutting stock problem."""
    B = 110  # roll width (bin size)
    w = [20, 45, 50, 55, 75]  # width (size) of orders (items)
    q = [48, 35, 24, 10, 8]  # quantitiy of orders
    s = []
    for j in range(len(w)):
        for i in range(q[j]):
            s.append(w[j])
    return s, B


def CuttingStockExample2():
    """CuttingStockExample2: create toy instance for the cutting stock problem."""
    B = 9  # roll width (bin size)
    w = [2, 3, 4, 5, 6, 7, 8]  # width (size) of orders (items)
    q = [4, 2, 6, 6, 2, 2, 2]  # quantitiy of orders
    s = []
    for j in range(len(w)):
        for i in range(q[j]):
            s.append(w[j])
    return s, B


def ReadingConfFile(conf_file_path):
    # print(conf_file_path)
    conf_file = file(conf_file_path, 'rb')
    reader = csv.reader(conf_file)
    rows = [row for row in reader]
    conf_file.close()

    for line in rows:
        print(line)

    capacity = rows[0][0]
    type_num = rows[1][0]
    dimension_num = rows[2][0]

    capacity_list = []
    for i in range(0, int(dimension_num)):
        capacity_list.append(int(capacity))

    type_ave_num_list = []
    for i in range(0, int(type_num)):
        type_ave_num_list.append(int(rows[3][i]))

    item_type_list = []
    for i in range(0, int(type_num)):
        item_type = []
        for j in range(0, int(dimension_num)):
            item_type.append(int(rows[i + 4][j]))
        item_type_list.append(item_type)

    item_list = []
    for i in range(int(type_num)):
        for j in range(type_ave_num_list[i]):
            item_list.append(item_type_list[i])

    return capacity_list, item_list


def FFD(s, B):
    """First Fit Decreasing heuristics for the Vector Packing Problem.

    Parameters:
        s - list with item widths
        B - bin capacity
    """
    tmp = []
    for i in range(len(B)):
        tmp.append(B[i])
    remain = [tmp]  # keep list of empty space per bin
    sol = [[]]  # a list ot items (i.e., sizes) on each used bin

    print('B: ' + str(B))

    # for n, item in enumerate(sorted(s, reverse=True)):
    for n, item in enumerate(s):
        print(str(n) + ': ' + str(item))
        flag = False
        for j, free in enumerate(remain):
            free_flag = True
            for i, each_free in enumerate(free):
                if each_free < item[i]:
                    free_flag = False
                    break

            if free_flag:
                for i in range(len(remain[j])):
                    remain[j][i] -= item[i]
                sol[j].append(item)
                flag = True
                break
        if not flag:  # does not fit in any bin
            sol.append([item])
            temp_B = [0] * len(B)
            for i in range(len(B)):
                temp_B[i] = B[i] - item[i]
            remain.append(temp_B)
    return sol


def solveCuttingStock(s, B):
    """solveCuttingStock: use Haessler's heuristic.

    Parameters:
        s - list with item widths
        B - bin capacity

    Returns a solution: list of lists, each of which with the cuts of a roll.
    """
    w = []  # list of different widths (sizes) of items
    q = []  # quantitiy of orders
    for item in sorted(s):
        if w == [] or item != w[-1]:
            w.append(item)
            q.append(1)
        else:
            q[-1] += 1

    t = []  # patterns
    m = len(w)
    # generate initial patterns with one size for each item width
    for i, width in enumerate(w):
        pat = [0] * m  # vector of number of orders to be packed into one roll (bin)
        pat[i] = int(B / width)
        t.append(pat)

    if LOG:
        print "sizes of orders=", w
        print "quantities of orders=", q
        print "roll size=", B
        print "initial patterns", t

    iter = 0
    K = len(t)
    master = Model("LP")  # master LP problem
    x = {}
    for k in range(K):
        x[k] = master.addVar(obj=1, vtype="I", name="x[%d]" % k)
    master.update()

    orders = {}
    for i in range(m):
        coef = [t[k][i] for k in range(K) if t[k][i] > 0]
        var = [x[k] for k in range(K) if t[k][i] > 0]
        orders[i] = master.addConstr(LinExpr(coef, var), ">", q[i], name="Order[%d]" % i)

    master.update()  # must update before calling relax()
    master.Params.OutputFlag = 0  # silent mode
    # master.write("MP" + str(iter) + ".lp")

    while 1:
        iter += 1
        relax = master.relax()
        relax.optimize()
        pi = [c.Pi for c in relax.getConstrs()]  # keep dual variables

        knapsack = Model("KP")  # knapsack sub-problem
        knapsack.ModelSense = -1  # maximize
        y = {}
        for i in range(m):
            y[i] = knapsack.addVar(obj=pi[i], ub=q[i], vtype="I", name="y[%d]" % i)
        knapsack.update()

        L = LinExpr(w, [y[i] for i in range(m)])
        knapsack.addConstr(L, "<", B, name="width")
        knapsack.update()
        # knapsack.write("KP"+str(iter)+".lp")
        knapsack.Params.OutputFlag = 0  # silent mode
        knapsack.optimize()
        if LOG:
            print "objective of knapsack problem:", knapsack.ObjVal
        if knapsack.ObjVal < 1 + EPS:  # break if no more columns
            break

        pat = [int(y[i].X + 0.5) for i in y]  # new pattern
        t.append(pat)
        if LOG:
            print "shadow prices and new pattern:"
            for i, d in enumerate(pi):
                print "\t%5d%12g%7d" % (i, d, pat[i])
            print

        # add new column to the master problem
        col = Column()
        for i in range(m):
            if t[K][i] > 0:
                col.addTerms(t[K][i], orders[i])
        x[K] = master.addVar(obj=1, vtype="I", name="x[%d]" % K, column=col)
        master.update()  # must update before calling relax()
        # master.write("MP" + str(iter) + ".lp")
        K += 1


    # Finally, solve the IP
    if LOG:
        master.Params.OutputFlag = 1  # verbose mode
    master.optimize()

    if LOG:
        print
        print "final solution (integer master problem):  objective =", master.ObjVal
        print "patterns:"
        for k in x:
            if x[k].X > EPS:
                print "pattern", k,
                print "\tsizes:",
                print [w[i] for i in range(m) if t[k][i] > 0 for j in range(t[k][i])],
                print "--> %d rolls" % int(x[k].X + .5)

    rolls = []
    for k in x:
        for j in range(int(x[k].X + .5)):
            rolls.append(sorted([w[i] for i in range(m) if t[k][i] > 0 for j in range(t[k][i])]))
    rolls.sort()
    return rolls


if __name__ == "__main__":

    if len(sys.argv) < 3:
        print('No ratio_standard and input_configuration_file specified!!!!')
        print('Please enter python script_name ratio_standard input_configuration_file')
        sys.exit()

    print('ratio_standard=' + str(sys.argv[1]))
    ratio_standard = float(sys.argv[1])
    input_file_name = str(sys.argv[2])
    conf_file_path = os.path.abspath(os.curdir) + '/' + str(sys.argv[2])
    print conf_file_path

    capacity_list, item_list = ReadingConfFile(conf_file_path)

    print('capacity_list: ' + str(capacity_list))
    print('item_list: ' + str(item_list))

    print "\n\nSolution of FFD:------------------------"
    ffd = FFD(item_list, capacity_list)
    print len(ffd), "bins"
    print(ffd)

    # print "\n\nCutting stock problem:-----------------------"
    # rolls = solveCuttingStock(s, B)
    # print len(rolls), "rolls:"
    # print rolls
