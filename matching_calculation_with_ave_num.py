#!/usr/bin/python
"""
This code is used to calculate the number of dominating matching type
"""
import sys, os

if len(sys.argv) < 4:
    print('No input_configuration_file, ratio_standard and VPSolver_name specified!!!!')
    print(
        'Please enter python script_name input_configuration_file ratio_standard VPSolver_name')
    sys.exit()

sdir = os.path.dirname(__file__)
if sdir != "": os.chdir(sdir)

""" Add VPSolver folders to path """

# add vpsolver folder to sys.path
sys.path.insert(0, "../")

# add vpsolver/bin folder to path
os.environ["PATH"] = "../bin" + ":" + os.environ["PATH"]

# add vpsolver/scripts folder to path
os.environ["PATH"] = "../scripts" + ":" + os.environ["PATH"]

""" Example """

## load all the vpsolver utils ##
from pyvpsolver import *
import random, os, csv

input_file_name = str(sys.argv[1])
conf_file_path = os.path.abspath(os.curdir) + '/' + str(sys.argv[1])
print conf_file_path
print('ratio_standard=' + str(sys.argv[2]))
ratio_standard = float(sys.argv[2])
print('VPSolver_name=' + str(sys.argv[3]))
VPSolver_name = sys.argv[3]

conf_file = file(conf_file_path, 'rb')
reader = csv.reader(conf_file)
rows = [row for row in reader]
conf_file.close()

for line in rows:
    print(line)

capacity = rows[0][0]
type_num = rows[1][0]
dimension_num = rows[2][0]

capacity_list = []
for i in range(0, int(dimension_num)):
    capacity_list.append(capacity)

type_ave_num_list = []
for i in range(0, int(type_num)):
    type_ave_num_list.append(int(rows[3][i]))

item_type_list = []
for i in range(0, int(type_num)):
    item_type = []
    for j in range(0, int(dimension_num)):
        item_type.append(int(rows[i + 4][j]))
    item_type_list.append(item_type)

print('capacity_list: ' + str(capacity_list))
print('type_num: ' + type_num)
print('dimension_num: ' + dimension_num)
print('type_ave_num_list: ' + str(type_ave_num_list))
print('item_type_list: ' + str(item_type_list))

type_set = set()
item_sum_dict = {}
item_sum_ratio_dict = {}
item_sum = 0

## Creating instanceA ##
instance = VBP(capacity_list, item_type_list, type_ave_num_list, verbose=False)

## Creating instance from a .vbp file ##
# instance = VBP.fromFile("vector_packing_test.vbp", verbose=False)

# solving an instance directly without creating AFG, MPS or LP objects
out, sol = VPSolver.script(VPSolver_name, instance, verbose=False)

## printing the solution ##
obj, patterns = sol
# print "Objective:", obj
# print "Patterns:", patterns
print_solution_vbp(obj, patterns)
print("")



